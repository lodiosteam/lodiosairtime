<?php 
	require __DIR__."/vendor/autoload.php";

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	/* Libries */
	use Monolog\Logger;
	use Monolog\Handler\StreamHandler;

	/* Application Specific */
	use LodiosAirtime\LodiosAirtime as LodiosAirtime;

	$app = new LodiosAirtime();
	$app->log = new Logger('LodiosAirtime');
    $logger = date('Y-m-d');
    $app->log->pushHandler(new StreamHandler(__DIR__."/logs/$logger.log", Logger::DEBUG));
    
    $app->log->info("LodiosAirtime loaded successfully");
    $app->execute();

?>