<?php

    /**
    * Author - Adekogbe Faith
    */
    namespace LodiosAirtime;

    use LodiosAirtime\Core\LODIOSapi as LODIOSapi;
    use LodiosAirtime\Core\Request as Request;
    use LodiosAirtime\Core\Response as Response;
    use LodiosAirtime\Core\BasicFunctions as BasicFunctions;

    class LodiosAirtime
    { 
        public function __construct() {
            $this->lodiosapi = new LODIOSapi();
            $this->response = new Response();
            $this->request = array();
        }

        public function execute() {
            $this->request = Request::handle();
            $this->response->handle($this);
        }
    }
?>
