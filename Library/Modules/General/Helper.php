<?php

    /**
    * Author - Adekogbe Faith 
    */
    namespace LodiosAirtime\Modules\General;

    use LodiosAirtime\Core\BasicFunctions as BasicFunctions;
    use LodiosAirtime\Modules\General\Api as Api;

    class Helper
    {
        public function __construct(&$data) {
            $this->data = &$data;
            $this->api = new Api();
        }

        public function handleOtherNetworks() {
            $token = $this->data->request['token'];
            $response = $this->api->get("v2/channel/subscription/updatetransaction/$token");
            return is_object($response) ? $response->response->status == 'success' : false;
        }

        public function handleMtn() {
            $msisdn = "0".substr($this->data->request['sender'], -10);
            $token = $this->data->request['token'];
            switch ($this->data->request['keyword']) {
                case 'video':
                    $response = $this->api->get("v2/channel/autorenewal/subscription/updatetransaction/$token/5/$msisdn");
                    //BasicFunctions::ddump($response);
                    break;
                case 'stop':
                    $response = $this->api->get("v2/channel/cancel/subscription/$token/$msisdn");
                    $this->data->request['channel'] = is_object($response) ? $response->response->channelcode : "";
                    break;
                default:
                    $response = false;
                    break;
            }
            return is_object($response) ? $response->response->status == 'success' : false;
        }

    }
?>
