<?php

    /**
    * Author - Adekogbe Faith 
    */
    namespace LodiosAirtime\Modules\General;

    use LodiosAirtime\Core\BasicFunctions as BasicFunctions;
    use LodiosAirtime\Modules\General\Helper as Helper;

    class Handler
    {
        public function __construct($data) {
            $this->data = $data;
            $this->message = 'Dear Customer, Your request was not accepted';
            $this->sender = '';
            $this->status = false;
            $this->helper = new Helper($data);
        }

        public function handle() {
            $receiver = (int)$this->data->request['receiver'];
            if($receiver == 20121)
                $this->status = $this->helper->handleOtherNetworks();
            elseif($receiver == 58124)
                $this->status = $this->helper->handleMtn();
            $this->sender = $this->data->request['sender'];
            if($this->status) {
                if($this->data->request['keyword'] == 'stop') {
                    $this->message = "Dear Customer, You have successfully unsubscribed to ".$this->data->request['channel']." Channel on lodios.com";
                }
                else {
                    $this->message = "Dear Customer, You have successfully subscribed to ".$this->data->request['channelcode']." Channel on Lodios.com. ";
                    if($receiver == 20121)
                        $this->message .= "Service cost N10.00 and is valid for 24 hours.";
                    elseif($receiver == 58124)
                        $this->message .= "Service cost N50.00 and is valid for 5 days.";
                }
            }
            return $this->response();
        }

        public function response() {
            $response = new \StdClass();
            $response->message = $this->message;
            $response->sender = $this->sender;
            $response->status = $this->status;
            return $response;
        }

    }
?>
