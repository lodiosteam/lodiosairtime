<?php

    /**
    * Author - Adekogbe Faith
    */
    namespace LodiosAirtime\Modules\General;

    use LodiosAirtime\Core\BasicFunctions as BasicFunctions;
    use LodiosAirtime\Core\LODIOSapi as LODIOSapi;

    class Api extends LODIOSapi
    {
        public function getThirdpartyPlan() {
            $response = $this->get('USSD/Insurance/ThirdParty/GetThirdPartyInfo');
            return is_object($response) ? ($response->IsSuccessful ? $response->ReturnedObject : false) : false;
        }

    }
?>
