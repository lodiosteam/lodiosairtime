<?php

    /**
    * Author - Adekogbe Faith
    */
    namespace LodiosAirtime\Modules\General;

    use LodiosAirtime\Core\BasicFunctions as BasicFunctions;

    class Validator
    {
        public static function validate(&$data) {
            $entry = $data->request['msgdata'];
            $entry = explode(" ", trim($entry));
            if(count($entry) != 3)
                return false;
            $entry[0] = strtolower($entry[0]);
            if(!in_array($entry[0], array('stop','rent','video')))
                return false;
            $data->request['keyword'] = $entry[0];
            $data->request['channelcode'] = $entry[1];
            $data->request['token'] = $entry[2];
            return true;
        }

    }
?>
