<?php

    /**
    * Author - Adekogbe Faith
    */
    namespace LodiosAirtime\Core;

    class Request
    {
        public static $fields = array('msgid' => true, 'msgdata' => true, 'recvtime' => true, 'operator' => true, 'receiver' => true, 'sender' => true);
        public static $request = array();
        public function __construct() {
            
        }

        public static function handle() {
            foreach (self::$fields as $key => $value) {
                $val = '';
                if (array_key_exists($key, $_REQUEST)) {
                    $val = $_REQUEST[$key];
                }
                self::$request[$key] = $val;
            }
            return self::$request;
        }
    }
?>
