<?php

    /**
    * Author - Adekogbe Faith
    */
    namespace LodiosAirtime\Core;

    use LodiosAirtime\Core\BasicFunctions as BasicFunctions;

    class LODIOSapi
    {
        const BASE = 'http://140.86.97.72/LodiosAPI/';

        public function __construct() {
            $this->headers = array(
                'Accept: application/json',
                'Content-Type:application/json'
            );
        }

        public function post($action,$data) {

            $url = self::BASE.$action;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20000);
            $output = curl_exec($ch);
            curl_close($ch);
            return json_decode($output);
        }

        public function get($action,$noBase = false,$debug = false) {

            $url = $noBase ? "" : self::BASE;
            $url .= $action;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20000);
            $output = curl_exec($ch);
            curl_close($ch);
            return $debug ? $output : json_decode($output);
        }

        static function urlify(array $data) {
            $postData = '';
            foreach($data as $key=>$value) { $postData .= $key.'='.urlencode($value).'&'; }
            rtrim($postData, '&');
            return $postData;
        }

    }
?>
