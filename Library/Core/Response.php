<?php

    /**
    * Author - Adekogbe Faith
    */
    namespace LodiosAirtime\Core;

    use LodiosAirtime\Core\BasicFunctions as BasicFunctions;
    use LodiosAirtime\Modules\General\Validator as Validator;
    use LodiosAirtime\Modules\General\Handler as Handler;
    use LodiosAirtime\Modules\General\Api as Api;

    class Response
    {
        public function __construct() {
            $this->data = new \stdClass();
            $this->message = 'Dear Customer, Your request was not accepted';
            $this->sender = '';
            $this->status = false;
            $this->api = new Api();
        }

        public function handle($data) {
            $data->log->info('REQUEST'.var_export($data->request, true));
            $this->data = $data;

            if(Validator::validate($data)) {
                $module = new Handler($this->data);
                $resp = $module->handle();
                $this->message = $resp->message;
                $this->sender = $resp->sender;
                $this->status = $resp->status;
            }
            $this->render();
        }

        private function render() {
            header("Content-type: text/plain");
            $message = urlencode($this->message);
            $this->status = $this->status ? 'success' : 'failed';
            $response2 = "Sender ID(20121),Status($this->status),Message($message),Destination($this->sender),Duration(24hrs)";
            echo $response2;
            
            $msisdn = "234".substr($this->sender,-10);
            $sender = $this->data->request['receiver'];
            $response = $this->api->get("http://83.138.190.168:8080/pls/vas2nets.inbox_pkg.schedule_sms?username=iyasolutions@vas2nets.com&password=iyasolutions123$&message=$message&receiver=$msisdn&sender=$sender&message_type=0", true, true);
            BasicFunctions::dump($response);
           $this->data->log->info('RESPONSE'.$response2.var_export($response,true));
            exit;
        }

    }
?>
