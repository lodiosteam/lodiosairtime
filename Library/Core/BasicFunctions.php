<?php

    /**
    * Author - Adekogbe Faith
    */
    namespace LodiosAirtime\Core;

    class BasicFunctions
    {
        public function __construct() {
            
        }

        public static function dump($mixed = null) {
            echo "<pre>";
            var_dump($mixed);
            echo "</pre>";
        }

        public static function ddump($mixed = null) {
            echo "<pre>";
            var_dump($mixed);
            echo "</pre>";
            die();
        }
    }
?>
